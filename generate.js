const cheerio = require('cheerio');
const axios = require('axios');
const fs = require('fs');

let monthlyData = [];
let availableTimes = [];

async function fetchDataForCod(cod) {
    const url = `https://ratings.fide.com/toparc.phtml?cod=${cod}`;

    try {
        const response = await axios.get(url);
        const $ = cheerio.load(response.data);

        var timeUnedited = $("#main-col > table:nth-child(1) > tbody > tr > td.contentheading");
        var timeText = timeUnedited.text();
        var indexS = timeText.indexOf("s");
        var indexDash = timeText.indexOf("-");
        var time = timeText.substring(indexS + 2, indexDash - 1);

        let players = [];

        for (let i = 2; i < 12; i++) {
            var playerSelector = `#main-col > table:nth-child(2) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(${i}) > td:nth-child(2)`;
            var eloSelector = `#main-col > table:nth-child(2) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(${i}) > td:nth-child(5)`;

            var playerText = $(playerSelector).text().replace('(GM)', '').trim();
            var playerNameParts = playerText.split(',');
            if (playerNameParts.length === 1) {
                playerName = playerNameParts[0].trim();
            } else if (playerNameParts.length === 2) {
                playerName = playerNameParts[1].trim() + ' ' + playerNameParts[0].trim();
            }

            var elo = parseInt($(eloSelector).text());

            players.push({ name: playerName, elo: elo });
        }

        monthlyData.push(players);
        availableTimes.push(time);

    } catch (error) {
        console.error(`Error fetching data for cod ${cod}: ${error}`);
    }
}

function saveDataToJson(data, fileName) {
    fs.writeFile(fileName, JSON.stringify(data, null, 2), (err) => {
        if (err) {
            console.error(`Error saving data to ${fileName}: ${err}`);
        } else {
            console.log(`Data saved to ${fileName}`);
        }
    });
}

async function fetchAllDataAndStore() {
    for (let cod = 57; cod <= 781; cod += 4) {
        await fetchDataForCod(cod);
    }
    saveDataToJson(monthlyData, 'monthlyData.json');
    saveDataToJson(availableTimes, 'availableTimes.json');
}

fetchAllDataAndStore();